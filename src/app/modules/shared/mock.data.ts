export const paramHeaders = [
    {
        id: 1,
        value: 'Gender',
        code: '0001',
        createdAt: new Date()
    },
    {
        id: 2,
        value: 'Status',
        code: '0002',
        createdAt: new Date()
    },
    {
        id: 3,
        value: 'Role',
        code: '0003',
        createdAt: new Date()
    }
];

export const paramDetail = [
    {
        id: 1,
        value: 'Male',
        code: 'M',
        headerId: 1,
        createdAt: new Date()
    },
    {
        id: 2,
        value: 'Male',
        code: 'M',
        headerId: 1,
        createdAt: new Date()
    },
    {
        id: 3,
        value: 'Enable',
        code: 'E',
        headerId: 2,
        createdAt: new Date()
    },
    {
        id: 4,
        value: 'Super Admin',
        code: 'super_admin',
        headerId: 3,
        createdAt: new Date()
    },
    {
        id: 5,
        value: 'Admin',
        code: 'admin',
        headerId: 3,
        createdAt: new Date()
    },
    {
        id: 6,
        value: 'Supervisor',
        code: 'supervisor',
        headerId: 3,
        createdAt: new Date()
    },
    {
        id: 7,
        value: 'Manager',
        code: 'manager',
        headerId: 3,
        createdAt: new Date()
    },

]