import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DBService {

  event = new EventEmitter();

  constructor() {
    console.log('user service')
   }


  public sendMessage(data:any):void{
    this.event.next(data);
  }
}
