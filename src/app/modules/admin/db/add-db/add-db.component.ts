import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DBService } from 'src/app/modules/admin/db/db.service';

@Component({
  selector: 'app-add-db',
  templateUrl: './add-db.component.html',
  styleUrls: ['./add-db.component.css']
})
export class AddDBComponent implements OnInit ,OnDestroy {

  form!: FormGroup;

  event = new EventEmitter();

  constructor(private fb: FormBuilder,private dbService:DBService) { 
  }



  ngOnInit(): void {
    this.dbService.event.subscribe(response=>{
      console.log(response,'add db');
    })

    this.form = this.fb.group({
      host: [undefined, [Validators.email, Validators.required]],
      password: [undefined, [Validators.required]],
      dbName: [undefined, [Validators.required]],
      port: [undefined],
    });

  }

  ngOnDestroy(){
    console.log('destroy it');
  }

}
