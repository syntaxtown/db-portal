import { Component, EventEmitter, OnInit } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { AddDBComponent } from '../add-db/add-db.component';
import { DBService } from '../db.service';


@Component({
  selector: 'app-manage-db',
  templateUrl: './manage-db.component.html',
  styleUrls: ['./manage-db.component.css']
})
export class ManageDBComponent implements OnInit {

  checked = false;
  loading = false;
  indeterminate = false;
  listOfData: any[] = [];
  listOfCurrentPageData: any[] = [];
  setOfCheckedId = new Set<number>();


  fixedColumn = false;

  tabs = ['Add User', 'List', 3];

  isVisible = false;
  isOkLoading = false;


  constructor(private modal: NzModalService,private dbService:DBService) { }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.listOfCurrentPageData.filter(({ disabled }) => !disabled).forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  sendRequest(): void {
    this.loading = true;
    const requestData = this.listOfData.filter(data => this.setOfCheckedId.has(data.id));
    console.log(requestData);
    setTimeout(() => {
      this.setOfCheckedId.clear();
      this.refreshCheckedStatus();
      this.loading = false;
    }, 1000);
  }


  ngOnInit(): void {
    this.listOfData = new Array(30).fill(0).map((_, index) => {
      return {
        id: index,
        name: `Edward King ${index}`,
        age: 32,
        address: `London, Park Lane no. ${index}`,
        disabled: index % 2 === 0,
        expand: false
      };
    });
  }

  showModal(): void {
    const modal: NzModalRef = this.modal.create({
      nzTitle: 'Add DB',
      nzContent: AddDBComponent,
      nzFooter: null
    });
  }


  afterClose(): void {
    
  }

}
