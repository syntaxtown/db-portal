import { Component } from '@angular/core';

@Component({
  selector: 'admin-base',
  template: `<router-outlet></router-outlet>`,
})
export class AdminBaseComponent {
}
