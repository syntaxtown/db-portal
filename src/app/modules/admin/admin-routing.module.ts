import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminBaseComponent } from './admin.base.component';
import { ManageDBComponent } from './db/manage-db/manage-db.component';

const routes: Routes = [
  {

    path: '',
    component: AdminBaseComponent,
    children: [
      {
        path: 'manage-db', component: ManageDBComponent
      },
      { path: '', redirectTo: 'manage-db', pathMatch: 'full', }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {

}
